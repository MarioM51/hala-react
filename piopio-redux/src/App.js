import React from 'react';
import { Container } from "react-bootstrap";
import Menu from './components/Menu';
import store from "./store";
import { Provider } from 'react-redux';
import ModalContainer from './components/ModalContainer';

function App() {
  //mt-5:       para darle un matgin-top de 5
  //Provider:   Lo usamos para poder acceder a nuestro store
  return (
    <Provider store={store}>
      <Menu />
      <Container className="mt-5">
        <h1 className="text-center">PioPio</h1>
      </Container>
      <ModalContainer>
        Formulario para agregar pios
      </ModalContainer>
    </Provider>
  );
}

export default App;
