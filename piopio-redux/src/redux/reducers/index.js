import { combineReducers } from 'redux';
import ModalsReducer from "./ModalsReducer";

export default combineReducers({
  modals: ModalsReducer
});