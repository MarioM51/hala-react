const initialProps = {
  isOpen_ModalAddPio: false
}
//al iniciar la app no le pasaremos nada por eso le ponemos como default el initialProps
export default function(state = initialProps, action) {
  switch (action.type) {
    // A que estado queremos cambiar 
    case "STATE_ADD_MODAL": // en este caso servira para abrir y cerrar el modal
      return {
        ...state,
        isOpen_ModalAddPio: action.payload //cambiamos el estado por lo que nos llegue
      };
    break;
    default:
      return state; 
    break;
  }
}