import React from 'react';
import { Container, Navbar, Button } from "react-bootstrap";
import LogoRedux  from "../assets/img/logo-redux.png";
import { useDispatch } from "react-redux";
import { openCloseAddPioModalAction } from "../redux/actions/ModalsActions";

function Menu() {

  const dispatch = useDispatch();
  const setOpenModalAddPio = state => { dispatch(openCloseAddPioModalAction(state)); }

  //mt-5 para darle un matgin-top de 5
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>
          <img alt="PioPio Redux" src={LogoRedux} width="30" height="30" className="d-inline-block align-top mr-4" />
          Pio-Pio Simulator
        </Navbar.Brand>
        <Button variant="outline-success" onClick={() => { setOpenModalAddPio(true)} }>Nuevo Tweet</Button>
      </Container>
    </Navbar>
  );
}

export default Menu;
