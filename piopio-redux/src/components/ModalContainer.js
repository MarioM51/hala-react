import React from 'react';
import { Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import { openCloseAddPioModalAction } from '../redux/actions/ModalsActions';

function ModalContainer(props) {
  const {children} = props;

  // useDispatch: es para acceder a nuestras acciones
  const dispatch = useDispatch();
  const setOpenModal = state => { dispatch(openCloseAddPioModalAction(state)); }

  // useSelector: para acceder a un valor en el store
  const isOpenModal = useSelector(state => state.modals.isOpen_ModalAddPio);

  return (
    <Modal show={isOpenModal} onHide={()=>setOpenModal(false)} size="large" centered>
      {children}
    </Modal>
  )
}
export default ModalContainer;