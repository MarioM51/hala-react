import React,  {useState, useEffect} from 'react';
import './App.css';
import Header from './components/header'
import { Container, Snackbar } from '@material-ui/core'
import AddPio from './components/AddPio'
import ListPios from './components/ListPios'

function App(name) {

  const [allPios, setAllPios] = useState([]);

  const [reloadPios, setReloadPios] = useState(false);

  const [snackBarProps, setSnackBarProps] = useState({
    open: false,
    text: ''
  });

  useEffect(() => {
    const allPiosStorage = localStorage.getItem('pios');
    const allPiosArray = JSON.parse(allPiosStorage);
    setAllPios(allPiosArray);
    setReloadPios(false);
  }, [reloadPios]); // CUIDADO: si no ponemos el  [] entra en loop

  const deletePio = (index) => {
    allPios.splice(index, 1);
    setAllPios(allPios);
    localStorage.setItem('pios', JSON.stringify(allPios));
    setReloadPios(true);
  }

  return (
    <Container className="tweet-simulator" maxWidth={false} >
      <Header />

      <ListPios allPios={allPios} deletePio={deletePio} />
      
      <AddPio setSnackBarProps={setSnackBarProps} setAllPios={setAllPios} allPios={allPios} />

      <Snackbar
        anchorOrigin = { {vertical: 'top', horizontal: 'right'} }
        open={snackBarProps.open}
        autoHideDuration={1000}
        message={<span id="message-id">{snackBarProps.text}</span>}
      />
    </Container>
  );
}

export default App;
