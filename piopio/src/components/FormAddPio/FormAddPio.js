import React, { useState } from 'react';
import { FormControl, FormGroup, TextField, Button } from '@material-ui/core'

import './FormAddPio.css'

function FormAddPio(props) {
  const { agregarPio } = props;

  const[formValue, setFormValue] = useState({ nombre: '', pio: '' });

  const onFormChange = (event) => {
    setFormValue({
      ...formValue,
      [event.target.name]: event.target.value // equivalente a name: "mi name" y a pio: "mi Pio"
    })
  }

  return (
    <div className="form-add-pio-container" >
      <h2>Crear Pio</h2>
      <form onSubmit={event => agregarPio(event, formValue)}  onChange={onFormChange}>
        <FormControl className="form-add-pio">
          <FormGroup className="name-user" >
            <TextField type="text" name="nombre" placeholder="Nombre de Usuario" margin="normal" />
          </FormGroup>

          <FormGroup>
            <TextField multiline rows="6" name="pio" placeholder="Escribe tu pio" margin="normal" />
          </FormGroup>

          <FormGroup className="btn-crear">
            <Button type="submit">Crear</Button>
          </FormGroup>

        </FormControl>
      </form>
    </div>
  )
}
export default FormAddPio;