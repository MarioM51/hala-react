import React, { useState } from 'react';
import { Fab } from '@material-ui/core'
//CUIDADO: import { AddIcon } from '@material-ui/icons/HomeIcon'
import AddIcon from '@material-ui/icons/Add';

import './AddPio.css'
import ModalContainer from "../ModalContainer";
import FormAddPio from "../FormAddPio";


function AddPio(props) {
  const { setSnackBarProps, setAllPios, allPios } = props;
  const [isOpenModal, setOpenModal] = useState(false);

  const openModal = () => { setOpenModal(true); }
  const closeModal = () => { setOpenModal(false); }

  const agregarPio = (event, formValue) => { 
    event.preventDefault();
    const {nombre, pio} = formValue;
    let allPiosTemp = allPios;
    if(!allPiosTemp) {
      allPiosTemp = [];
    }
    if(!nombre || !pio) {
      setSnackBarProps({open: true, text: 'ADVERTENCIA: Nombre y/o Pio vacio'});
    } else {
      formValue.time = new Date();
      allPiosTemp.push(formValue);
      localStorage.setItem("pios", JSON.stringify(allPiosTemp));
      setAllPios(allPiosTemp);
      setSnackBarProps({open: true, text: 'MENSAJE: Pio Creado correctamente'});
      closeModal();
    }
  }


  return (
    <div className="add-pio" >
      <Fab onClick={openModal} className="open-modal" color="primary" aria-label="add">
        <AddIcon />
      </Fab>

      <ModalContainer isOpenModal={isOpenModal} closeModal={closeModal}>
        <FormAddPio agregarPio={agregarPio} />
      </ModalContainer>
    </div>
  )
}
export default AddPio;