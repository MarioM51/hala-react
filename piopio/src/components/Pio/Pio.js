import React from 'react';
import { Card, CardContent } from '@material-ui/core'
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone'
import moment from 'moment';

import './Pio.css'

function Pio(props) {
  const { pio:{nombre, pio, time}, deletePio, index } = props;

  console.log(nombre, pio, time);
  
  return (  
    <Card className="pio-container">
      <CardContent className="pio">
        <header>
          <h5>{nombre}</h5>
          <DeleteTwoToneIcon onClick={() => deletePio(index) } />
        </header>
        <p>{pio}</p>
        <div className="pio-footer">
          {moment(time).format("YYYY/MM/DD HH:mm")}
        </div>
      </CardContent>
    </Card>
  )
}
export default Pio;