import React from 'react';
import { Grid } from '@material-ui/core'
import Pio from '../Pio'

import './ListPios.css'


function ListPios(props) {

  const { allPios, deletePio } = props;
  

  console.log(allPios);

  if(!allPios || allPios.length===0 ) {
    return (
      <div className="pio-list-emply" >
        No Ningun Pio agregado
      </div>
    )
  }

  return (  
    <Grid container spacing={3} className="pio-list">
      {allPios.map((pio, key) => (
        <Grid key={key} item xs={4} >
          <Pio pio={pio} deletePio={deletePio} index={key} />
        </Grid>
      ))}
    </Grid>
  )
}
export default ListPios;