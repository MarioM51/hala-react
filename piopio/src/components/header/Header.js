import React from 'react';
import './Header.css';
import Logo from '../../assets/img/logo.png';

function Header(props) {
  return (
    <div className="header" alt="logo">
      <img src={Logo} alt="logo"/>
      <h1>PioPio-Simulador</h1>
    </div>
  );
}
export default Header;
