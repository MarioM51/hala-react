import React, { useState, useEffect } from 'react';
import TopMenu from './components/TopMenu';
import useFetch from './hooks/useFetch';
import { API_PRODUCTS, STORAGE_PRODUCT_CAR } from './utils/Constants';
import Products from './components/Products/Products';
import { ToastContainer, toast } from 'react-toastify';

function App() {
  const consult = useFetch(API_PRODUCTS, null);
  const [ productsCart, setProductsCart ] = useState([]);

  const getInitialProductsCar = () => {
    const idsProducts = localStorage.getItem(STORAGE_PRODUCT_CAR);
    if(idsProducts) {
      const idsTemp = idsProducts.split(","); //recuperamos string (ej '1,2,3') lo convertimos a array
      setProductsCart(idsTemp);
    } else {
      setProductsCart([]);
    }
  }

  useEffect(() => {
    getInitialProductsCar();
  }, [])

  const addproductToCart = (id, name) => {
    const idsProducts = productsCart;
    idsProducts.push(id);
    setProductsCart(idsProducts);
    localStorage.setItem(STORAGE_PRODUCT_CAR, idsProducts);
    getInitialProductsCar(); // cuando se agrega un producto cambia el icono de carrito
    toast.success(`Agregado ${name}`);
  }

  return (
    <div className="App">
      <TopMenu productsCart={productsCart} products={consult} getInitialProductsCar={getInitialProductsCar} />
      <Products products={consult} addproductToCart={addproductToCart} />

      <ToastContainer 
        position="bottom-left"
        autoClose={4000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange={false}
        draggable
        pauseOnHover={false}
      />
    </div>
  );
}

export default App;
