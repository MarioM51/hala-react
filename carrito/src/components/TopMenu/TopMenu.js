import React from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import './TopMenu.scss';
import { ReactComponent as Logo } from '../../assets/svg/logo-carrito.svg';
import Car from '../Car/Car';


function TopMenu(props) {
  const { productsCart, products, getInitialProductsCar } = props;
  return (
    <Navbar bg="dark" variant="dark" className="top-menu">
      <Container>
        <BrandNav />
        { /*  ------------------ MENU ---------------- */ }
        <MenuNav />
        <Car productsCart={productsCart} products={products} getInitialProductsCar={getInitialProductsCar} />
      </Container>
    </Navbar>
  );
}

function BrandNav() {
  return (
    <Navbar.Brand>
      <Logo />
      <h2>La casa de los helados</h2>
    </Navbar.Brand>
  );
}

function MenuNav() {
  return (
    <Nav className="mr-auto">
      <Nav.Link href="#">Aperitivos</Nav.Link>
      <Nav.Link href="#">Helados</Nav.Link>
      <Nav.Link href="#">Mascotas</Nav.Link>
    </Nav>
  );
}

export default TopMenu;
