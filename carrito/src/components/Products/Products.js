import React from 'react';
import './Products.scss';
import { Container, Row } from 'react-bootstrap';
import Loading from '../Loading/Loading';
import Product from '../Product/Product';

function Products(props) {
  const { products: {result, loading, error}, addproductToCart } = props;

  return (
    <Container className="products">
      <Row>
        { loading || !result ? (
          <Loading />
        ) : (
          result.map((product, index) => (
            <Product product={product} addproductToCart={addproductToCart} />
          ))
        )}
      </Row>
    </Container>
  );
}
export default Products;
