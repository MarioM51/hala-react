import React, { useState, useEffect } from 'react';
import './Car.scss';
import { Button } from 'react-bootstrap';

import { ReactComponent as CarEmpty } from '../../assets/svg/cart-empty.svg';
import { ReactComponent as CarFull } from '../../assets/svg/cart-full.svg';
import { ReactComponent as CarClose } from '../../assets/svg/close.svg';
import { ReactComponent as CarGarbage } from '../../assets/svg/garbage.svg';
import { removeArrayDuplicates, countDuplicatesItemArray, removeItemArray } from '../../utils/arrayFuncs';
import { BASE_PATH } from '../../utils/Constants';
import { STORAGE_PRODUCT_CAR } from '../../utils/Constants';

function Car(props) {
  const { productsCart, products, getInitialProductsCar } = props;
  const [isCarOpen, setIsCarOpen] = useState(false);
  const widthCarContent = isCarOpen ? 400 : 0; //tamaño del content del carro
  const [ singleProductsCar, setSingleProductsCar ] = useState([]);
  const [ totalPrice, setTotalPrice ] = useState(0);

  useEffect(() => { //cargar los productos solo uno de cada tipo
    const allProductsId = removeArrayDuplicates(productsCart);
    setSingleProductsCar(allProductsId);
  }, [productsCart])

  useEffect(() => { //calcular el total del carrito
    const productData = [];
    let total = 0;

    const allProductsId = removeArrayDuplicates(productsCart);
    allProductsId.forEach((productId) => {
      const quantity = countDuplicatesItemArray(productId, productsCart);
      const productValue = {
        id: productId,
        quantity: quantity
      };
      productData.push(productValue);
    });
    if(!products.loading && products.result) {
      products.result.forEach(product => {
        productData.forEach(item => {
          if(product.id == item.id) {
            const totalByProduct = product.price * item.quantity;
            total = total + totalByProduct
          }
        });
      });
    }
    setTotalPrice(total);
  }, [productsCart, products]);

  const openCar = () => {
    setIsCarOpen(true);
    document.body.style.overflow = "hidden"; // ocultamos el scroll para que no suba y baje
  }

  const closeCar = () => {
    setIsCarOpen(false);
    document.body.style.overflow = "scroll"; // ocultamos el scroll para que no suba y baje
  }

  const emptyCar = () => {
    localStorage.removeItem(STORAGE_PRODUCT_CAR);
    getInitialProductsCar();
  }

  const increseQuantity = (id) => {
    const arrayItemCar = productsCart;
    arrayItemCar.push(id);
    localStorage.setItem(STORAGE_PRODUCT_CAR, arrayItemCar);
    getInitialProductsCar();
  }

  const decreseQuantity = (id) => {
    const arrayItemCar = productsCart;
    localStorage.setItem(STORAGE_PRODUCT_CAR, removeItemArray(arrayItemCar, id.toString()));
    getInitialProductsCar();
  }
  
  return (
    <>
      <Button variant="link" className="car">
        {productsCart.length>0 ? ( //si hay productos mustra carrito lleno
          <CarFull onClick={openCar} />
        ) : (                       // mustra carrito vacio por que no hay productos
          <CarEmpty onClick={openCar} />
        )}
      </Button>
      <div className="car-content" style={{width: widthCarContent}}>
        <CarContentHeader closeCar={closeCar} emptyCar={emptyCar} />
        <div className="car-content__products">
          {singleProductsCar.map((idProductCar, index) => (
              <CarContentProducts key={index} products={products} productsCart={productsCart} 
              idProductCar={idProductCar} increseQuantity={increseQuantity}  decreseQuantity={decreseQuantity} />
          ))}
        </div>
        <CarContentFooter totalPrice={totalPrice} />
      </div>
    </>
  );
}


function CarContentHeader(props) {
  const { closeCar, emptyCar } = props

  return (
    <div className="car-content__header">
      <div>
        <CarClose onClick={closeCar} />
        <h2>Carrito</h2>
      </div>
      <Button variant="link">
        Vaciar <CarGarbage onClick={emptyCar} />
      </Button>
    </div>
  )
}

function CarContentProducts(props) {
  const {products: { loading, result }, productsCart, idProductCar, increseQuantity, decreseQuantity} = props;
  
  if(!loading && result) {
    return result.map((product, index) => { // en todos los productos buscamos el productoId agregado en el carrito
      if(idProductCar == product.id) {     //se encontro un id del carrito en la lista de todos los productos
        //contamos cuantas veces esta el mismo producto en el carrito
        const quantity = countDuplicatesItemArray(product.id, productsCart);
        return (
          <RenderProduct key={index} product={product} quantity={quantity} increseQuantity={increseQuantity} decreseQuantity={decreseQuantity} />
        )
      }
    });
  }
  return null;
}

function RenderProduct(props) {
  const { product, quantity, increseQuantity, decreseQuantity } = props

  return (
    <div className="car-content__product" style={{color: 'white'}}>
      <img src={`${BASE_PATH}/${product.image} `} alt={product.name} style={{width: '50px'}} />
      <div className="car-content__product-info">
        <div><h3>{product.name.substr(0, 25)}</h3></div>
        <p>{product.price.toFixed(2)} $ / ud.</p>
        <div>
          <p>En Carrito: {quantity} ud.</p>
          <div>
            <button onClick={() => increseQuantity(product.id) } >+</button>
            <button onClick={() => decreseQuantity(product.id) } >-</button>
          </div>
        </div>
      </div>
    </div>
  )
}

function CarContentFooter(props) {
  const { totalPrice } = props
  return (
    <div className="car-content__footer">
      <div>
        <p>Total Aproximado: </p>
        <p>{totalPrice.toFixed(2)} $</p>
      </div>
      <Button>Tramitar Pedido</Button>
    </div> 
  )
}

export default Car;
