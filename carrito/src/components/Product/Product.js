import React from 'react';
import { Col, Card, Button } from 'react-bootstrap';
import { BASE_PATH } from '../../utils/Constants';

import './Product.scss';

function Product(props) {
  const { product: { id, image, name, extraInfo, price }, addproductToCart } = props;

  return (
    <Col className="product">
      <Card.Img variant="top" src={`${BASE_PATH}/${image}`}></Card.Img>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{extraInfo}</Card.Text>
        <Card.Text>{price} $ / unidad</Card.Text>
        <Button onClick={() => { addproductToCart(id, name) } }>Añadir al carrito</Button>
      </Card.Body>
    </Col>
  );
}
export default Product;
