import React from 'react';
import './App.css';
import { Layout } from 'antd'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import MenuTop from './components/MenuTop';

import Home from './pages/Home';
import Movie from './pages/movie';
import NewMovies from './pages/NewMovies';
import PopularMovies from './pages/PopularMovies';
import SearchMovies from './pages/search';
import Error404 from './pages/error404';

function App() {

  const { header, content } = Layout;

  // el Switch sirve para que solo carge una, si ahi mas iguales no las cargara
  return (
    <Layout>
      <Router>
        <header style={{zIndex: '3'}}>
          <MenuTop />
        </header>
        <content>
          <Switch>
            <Route path="/" exact={true} >
              <Home />
            </Route>
          
            <Route path="/movie/:id" exact={true} >
              <Movie />
            </Route>
          
            <Route path="/new-movies" exact={true} >
              <NewMovies />
            </Route>
          
            <Route path="/populars" exact={true} >
              <PopularMovies />
            </Route>
          
            <Route path="/search" exact={true} >
              <SearchMovies />
            </Route>
          
            <Route path="*">
              <Error404 />
            </Route>

          </Switch>
        </content>

      </Router>
    </Layout>
  );
}

export default App;
