import React from 'react';

import useFetch from "../hooks/useFetch";
import {API_URL, API_TOKEN, API_LANG} from '../utils/Constants';

import SliderMovies from "../components/SliderMovies";
import MovieList from "../components/MovieList";
import Footer from "../components/Footer";

import { Row, Col } from 'antd';


export default function Home() {

  const urlMovies = `${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=1`;
  const movies = useFetch(urlMovies);

  const urlPopulars = `${API_URL}/movie/popular?${API_TOKEN}&${API_LANG}&page=1`;
  const popularsResp = useFetch(urlPopulars);

  const urlTop = `${API_URL}/movie/top_rated?${API_TOKEN}&${API_LANG}&page=1`;
  const topResp = useFetch(urlTop);
  
  return (
    <>
      <SliderMovies movies={movies} />
      <Row>
        <Col span="12">
          <MovieList title="Peliculas populares" movies={popularsResp} />
        </Col>

        <Col span="12">
          <MovieList title="Top peliculas mas puntuadas" movies={topResp} />
        </Col>

      </Row>
      <Footer />
    </>
  );
}

