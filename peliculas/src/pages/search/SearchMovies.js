import React, { useEffect, useState } from 'react';
import { Row, Col, Input } from 'antd';
import { withRouter } from 'react-router-dom';
import queryString  from 'query-string';
//import MovieCatalog from '../../components/MovieCatalog/MovieCatalog';
import { API_URL, API_TOKEN, API_LANG } from '../../utils/Constants';
import MovieCatalog from '../../components/MovieCatalog/MovieCatalog';
import Footer from '../../components/Footer';
import './SearchMovies.scss';

function SearchMovies(props) {
  //console.log(props); //al hacer wrapper podemos ver el historial y otras cosas 
  const { location, history } = props;
  const [ movieList, setMovieList ] = useState([]);
  const [ searchValue, setSearchValue ] = useState("");

  useEffect(() => {
    (async () => {
      const searchQValue = queryString.parseUrl(location.search); // capturamos datos de la url
      const { s } = searchQValue.query; //capturamos parametro s ej. dom.com?s=algo en este caso "algo"
      
      if(s) { 
        const response = await fetch(
          `${API_URL}/search/movie?${API_TOKEN}&${API_LANG}&query=${s}&page=1`
        );
        const movies = await response.json();
        setMovieList(movies); // PENDIENTE: NO SE QUE PASA imprime null el log pero al final si no asigna
      }
      else { 
        setSearchValue("");
      }
    })();
  }, [location.search]);

  const onChangeSearch = (e) => {
    const urlParams = queryString.parse(location.search); //consegimos params de la url
    urlParams.s = e.target.value;                         //Los actualizamos
    const params = queryString.stringify(urlParams);      //actualizamos en la url
    history.push(`?${params}`);
    setSearchValue(urlParams.s);                        //cambiamos el valor en nuestro input=buscador
  }
  
  return (
    <Row>
      <Col span="12" offset="6" className="search">
        <h1>Busca tu pelicula</h1>
        <Input value={searchValue} onChange={onChangeSearch} />
      </Col>
      {movieList.results ? (
        <Row span="24">
          <MovieCatalog listMovies={movieList} />
        </Row>
      ) : (
        <Col span="24">
          
        </Col>
      ) }     

      <Col span={24} >
        <Footer />
      </Col>

    </Row>

  );
}

export default withRouter(SearchMovies); //wrapper a SearchMovies