import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import { API_URL, API_TOKEN, API_LANG } from '../utils/Constants';
import Footer from '../components/Footer';
import Loading from '../components/Loading';
import MovieCatalog from '../components/MovieCatalog/MovieCatalog';
import PaginationMovies from '../components/Pagination/PaginationMovies';

function NewMovies() {
  const [listMovies, setListMovies] = useState([])
  const [page, setPage] = useState(1);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${API_URL}/movie/now_playing?${API_TOKEN}&${API_LANG}&page=${page}`);
      const movies = await response.json();
      setListMovies(movies);
    })();
  }, [page]);

  const onChargePage = (pageIn) => {
    setPage(pageIn);
  }

  return (
    <Row>
      <Col span="24" style={{ textAlign: 'center', marginRight: 25 }}>
        <h1 style={{fontSize: '30px', fontWeight: 'bold'}}>
          Ultimos Lanzamientos
        </h1>
      </Col>
      
      {listMovies.results ? ( // ternario para mostrar contenido con ternario
    <>
      <MovieCatalog listMovies={listMovies} />
      <Col span="24">
        <PaginationMovies currentPage={listMovies.page} totalItems={listMovies.total_results} onChargePage={onChargePage} />
      </Col>
    </>
      ) : (
        <Col span="24" >
          <Loading />
        </Col>
      ) }
      <Col span="24" >
      
        <Footer />
      </Col>
    </Row>
  );
}

export default NewMovies;
