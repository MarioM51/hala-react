import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import Loading from '../../components/Loading';
import useFetch from '../../hooks/useFetch';
import { API_URL, API_TOKEN, API_LANG, API_IMAGES } from '../../utils/Constants';
import { Row, Col, Button } from 'antd';
import moment from 'moment';

import './Movie.scss';
import ModalVideo from '../../components/ModalVideo/ModalVideo';

function Movie() {
  const { id } = useParams();
  
  const urlInfo = `${API_URL}/movie/${id}?${API_TOKEN}&${API_LANG}&page=1`;
  const movieInfo = useFetch(urlInfo);
  
  if (movieInfo.isLoading || !movieInfo.result) {
    return <Loading />
  }

  return <RenderMovie movie={movieInfo.result} />
}

function RenderMovie(props) {
  console.log("Movie info");
  console.log(props);
  const { movie:{poster_path, backdrop_path} } = props;
  const backdropPath = API_IMAGES + backdrop_path;

  return (
    <div className="movie-details" style={{backgroundImage: `url(${backdropPath})`}}>
      <div className="movie-details__dark" />
      <Row>
        <Col span="8" offset="3" className="movie-details__poster">
            <PosterMovie image={poster_path} />        
        </Col>
        <Col span="10" className="movie-details__info">
          <MovieInfo movieInfo={props.movie} />
        </Col>
      </Row>
    </div>
  );
}


function PosterMovie(props) {
  const {image} = props;
  const posterPath = API_IMAGES + image;

  return <div style={{backgroundImage: `url('${posterPath}')`}}></div>;
}

function MovieInfo(props) {
  const {movieInfo: {id, title, release_date, overview, genres} } = props;
  
  const [ isVisible, setVIsVisible ] = useState(false);

  const keyVideoMovie = useFetch(`${API_URL}/movie/${id}/videos?${API_TOKEN}&${API_LANG}`);

  const openModal = () => setVIsVisible(true);
  const closeModal = () => setVIsVisible(false);

  const renderButton = () => {
    if(keyVideoMovie.result) {
      if(keyVideoMovie.result.results.length > 0) {
        const info = keyVideoMovie.result.results[0];
        return  ( 
          <>  
            <Button onClick={openModal} >Ver Trailer</Button>
            <ModalVideo videoKey={info.key} videoPlataform={info.site} isOpen={isVisible} close={closeModal} />
          </> );
      }
    }
  };

  return (
  <>
    <div className="movie-details__info-header">
      <h1>
        {title}
        <span>{ moment(release_date, 'YYYY-MM-dd').format('YYYY') }</span>
      </h1>
      { renderButton() }
    </div>
    <div className="movie-details__info-content">
      <h3>General</h3>
      <p>{overview}</p>

      <h3>Generos</h3>
      <ul>
        {genres.map(genre => (
          <li key={genre.id}>{genre.name}</li>
        ))}
      </ul>
    </div>
  </>);
}

export default Movie;


