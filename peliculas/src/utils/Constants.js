
export const API_URL = "https://api.themoviedb.org/3";
export const API_TOKEN = "api_key=78785c5aa3ef8df607e2b32524f6fa42";
export const API_LANG = "language=es-Es";
export const API_IMAGES = "https://image.tmdb.org/t/p/original";

export const buildUrl = (url) => {
  return `${API_URL}/movie/${url}?${API_TOKEN}&${API_LANG}`;
}