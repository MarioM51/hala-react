import { useEffect, useState } from 'react';

export default function useFetch(url, options) {

  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [result, setResult] = useState(null);

  useEffect(() => {
    (async () => {
      try {
        const res = await fetch(url, options); // mandamos la peticion
        const resJson = await res.json(); // la respuesta la pasamos a json
        setResult(resJson); // guardamos el resultado
        setIsLoading(false); // indicamos que ya acabo de cargar
      } catch (error) {
        setIsLoading(false);
        setError(error);
      }
    })(); // CUIDADO CON LOS ULTIMOS "()" SI LOS OLVIDAMOS HAY PROBLEMAS CON LA IMPORTACION
  }, [url, options]);

  return {isLoading, result, error}; //CUIDADO SON LLAVES NO PARENTESIS
}
