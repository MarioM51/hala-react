import React from 'react';

import './MovieCatalog.scss';
import { Col, Card } from 'antd';
import { EyeFilled } from '@ant-design/icons';
import { API_IMAGES } from '../../utils/Constants';
import { Link } from 'react-router-dom';

function MovieCatalog(props) {
  console.log("propsd");
  console.log(props);
  const {listMovies:{ results} } = props;

  return results.map(movie => (
    <Col key={movie.id} xs={4} className="movie-catalog">
      <MovieCard movie={movie} />
    </Col>
  ));
}

function MovieCard(props) {
  const { movie: {id, title, poster_path} } = props;
  const { Meta } = Card;
  const poster_url = API_IMAGES + poster_path;
  
  //hoverable: cuando pase el raton ensima muestre la mano
  return (
    <Link to={`/movie/${id}`} >
      <Card hoverable style={{width: '200px'}} cover={<img alt={title} src={poster_url}/>} actions={[<EyeFilled />]}>
        <Meta title={title} />
      </Card>
      
    </Link>
  );
  
}

export default MovieCatalog;
