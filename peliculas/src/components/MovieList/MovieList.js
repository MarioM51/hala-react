import React from 'react';
import { List, Button, Avatar } from 'antd';
import { RightOutlined } from '@ant-design/icons';
import Loading from '../Loading';
import { API_IMAGES } from '../../utils/Constants'
import { Link } from 'react-router-dom';

import './MovieList.scss';

function MovieList(props) {
  const { title, movies } = props;

  if(movies.isLoading || !movies.result ) {
    return  <Loading />;
  }

  return (
    <List dataSource={movies.result.results} className="movie-list" 
          renderItem={movie => <RenderMovie movie={movie} />} size="default" header={<h2>{title}</h2>} bordered >
      
    </List>
  );
}

function RenderMovie(props) {
  const { movie:{id, title, poster_path} } = props;
  const posterPath = API_IMAGES + poster_path;

  return (
    <List.Item className="movie-list??movie">
      <List.Item.Meta avatar={ <Avatar src={posterPath} /> }  
                      title ={ <Link to={`/movie/${id}`}>{title}</Link>} />
      
      <Link to={`/movie/${id}`}>
        <Button type="primary" icon={<RightOutlined />}></Button>
      </Link>

    </List.Item>
  );
}


export default MovieList;