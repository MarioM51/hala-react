import React from 'react';
import './SliderMovies.scss';
import {Button, Carousel } from 'antd';
import { Link } from 'react-router-dom';
import Loading from '../Loading';
import { API_IMAGES } from '../../utils/Constants'


function SliderMovies(props) {
  const { movies } = props;
  
  if(movies.isLoading || !movies.result ) {
    return  <Loading />;
  }

  return (
    <Carousel autoplay className="slider-movies">
      {movies.result.results.map(movie => ( 
        <Movie key={movie.id} movie={movie} />) 
      )}
    </Carousel>
  );
}

export default SliderMovies;

function Movie(props) {

  const {movie: {id, backdrop_path, title, overview} } = props;
  const backdrop_url = API_IMAGES + backdrop_path;

  return (
    <div className="slider-movies__movie" style={{ backgroundImage: `url('${backdrop_url}')`}}>
      <div className="slider-movies__movie-info">
        <div>
          <h2>{title}</h2>
          <p>{overview}</p>
          <Link to={`movie/${id}`}>
            <Button type="primary">Ver Mas</Button>
          </Link>
        </div>
      </div>
    </div>
  );

}