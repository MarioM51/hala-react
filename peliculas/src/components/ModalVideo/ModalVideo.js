import React, { useState, useEffect } from 'react';

import { Modal } from 'antd';
import ReactPlayer from 'react-player';

import './ModalVideo.scss';

function ModalVideo(props) {

  const {videoKey, videoPlataform, isOpen, close} = props;

  const [ urlVideo, setUrlVideo] = useState(null);

  useEffect(() => {
    switch (videoPlataform) {
      case 'YouTube':
        setUrlVideo('https://youtu.be/' + videoKey);
      break;

      case 'Vimeo':
        setUrlVideo('https://vimeo.com/' + videoKey);
      break;
    
      default:
        alert("No plataforma de video no soportada");
        break;
    }
  }, [videoPlataform, videoKey])

  return (
    <Modal className="modal-video" visible={isOpen} centered onCancel={close} footer={false} >
      <ReactPlayer url={urlVideo} controls />
    </Modal>
  )
}

export default ModalVideo;
