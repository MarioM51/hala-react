import React from 'react';
import Pagination from 'rc-pagination';

import './PaginationMovies.scss';

function PaginationMovies(props) {
  const { currentPage, totalItems, onChargePage } = props;
  console.log(currentPage, totalItems, onChargePage);
  

  return (
    <Pagination className="pagination" current={currentPage} total={totalItems} pageSize={20} onChange={onChargePage} />
  );
}


export default PaginationMovies;
