import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

import { ReactComponent as Logo } from '../../assets/images/original.svg';

import './MenuTop.scss';

function MenuTop() {

  return (
    <div className="menu-top">
      <div className="logo">
        <Logo />
      </div>
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}
         style={{ lineheight: '64px'}}>
        
        <Menu.Item key="1">
          <Link to="/">Home</Link>
        </Menu.Item>

        <Menu.Item key="2">
          <Link to="/new-movies">Estrenos</Link>
        </Menu.Item>

        <Menu.Item key="3">
          <Link to="/populars">Populares</Link>
        </Menu.Item>

        <Menu.Item key="4">
          <Link to="/search">Buscador</Link>
        </Menu.Item>

      </Menu>
    </div>
  );
}

export default MenuTop;
